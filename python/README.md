# tiny-raytracer

## requirements
- numpy
- pillow

## basic usage
- set the image resolution in the main function
- set "iterations" to a value > 1 to enable averaging over multiple images (analogous to generating multiple samples per pixel)
- set REFLECTION_RECURSION_DEPTH to determine the number of recursions for reflection and refraction rays