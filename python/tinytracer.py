from PIL import Image
import numpy as np
import time
import random


class Material(object):
    def __init__(self, color=[0., 0., 0.], specular_exponent=0, albedo=[1.0, 0, 0, 0], refractive_index=1.0):
        self.diffuse_color = color
        self.albedo = albedo
        self.specular_exponent = specular_exponent
        self.eta = refractive_index


class Ray(object):

    def __init__(self, o, d):
        self.orig = np.array((o[0], o[1], o[2]))
        self.dir = np.array((d[0], d[1], d[2]))


class Light(object):
    def __init__(self, pos, intensity):
        self.pos = np.array(pos)
        self.intensity = intensity


class Sphere(object):

    def __init__(self, c, r, material):
        self.material = material
        self.center = np.array((c[0], c[1], c[2]))
        self.radius = r
        self.radsq = self.radius*self.radius

    def ray_intersect(self, ray, t0):

        L = self.center - ray.orig
        tca = L.dot(ray.dir)
        d2 = L.dot(L) - tca * tca

        if d2 > self.radsq:
            return False, t0

        thc = np.sqrt(self.radsq - d2)
        t0 = tca - thc
        t1 = tca + thc

        if t0 < 0:
            t0 = t1

        if t0 < 0:
            return False, t0

        return True, t0


def reflect(I, N):
    return I - N * 2 * I.dot(N)


def refract(I, N, eta):
    cosi = - np.clip(I.dot(N), -1., 1.)
    etai = 1
    etat = eta
    n = N
    if cosi < 0:
        cosi = -cosi
        etai = eta
        etat = 1
        n = -N

    e = etai / etat
    k = 1 - e*e*(1-cosi*cosi)
    if k < 0:
        return np.array([0, 0, 0])
    else:
        return np.array(np.multiply(I, e) + np.multiply(n, (e * cosi - np.sqrt(k))))


def scene_intersect(ray, spheres):
    spheres_dist = np.inf
    hit = np.array([0, 0, 0])
    N = np.array([0, 0, 0])
    material = Material([0, 0, 0], 1, [1, 0.0, 0.0, 0.0], 1.0)

    for sphere in spheres:
        dist_i = 0
        res, dist_i = sphere.ray_intersect(ray, dist_i)
        if res and dist_i < spheres_dist:
            spheres_dist = dist_i
            hit = ray.orig + ray.dir * dist_i
            N = hit - sphere.center
            N /= np.linalg.norm(N)
            material = sphere.material

    checkerboard_dist = np.inf
    if abs(ray.dir[1]) > 0.001:
        d = - (ray.orig[1]+4) / ray.dir[1]
        pt = ray.orig + np.multiply(ray.dir, d)

        if d > 0 and abs(pt[0]) < 10 and pt[2] < -10 and pt[2] > -30 and d < spheres_dist:
            checkerboard_dist = d
            hit = pt
            N = np.array([0, 1, 0])
            material = mirror

            # if (int(0.5 * hit[0]+1000) + int(0.5 * hit[2])) % 2 == 1:
            #     material.diffuse_color = [0.3, 0.3, 0.3]
            # else:
            #     material.diffuse_color = [0.3, 0.2, 0.1]

    return np.min([spheres_dist, checkerboard_dist]) < 1000, hit, N, material


def cast_ray(ray, spheres, lights, depth):
    global numrays
    numrays += 1
    res, hit, N, material = scene_intersect(ray, spheres)

    if depth > REFLECTION_RECURSION_DEPTH or not res:
        return [0.2, 0.7, 0.8]
    else:

        if material.albedo[2] != 0:
            reflect_dir = reflect(ray.dir, N)
            reflect_dir /= np.linalg.norm(reflect_dir)

            if reflect_dir.dot(N) < 0:
                reflect_orig = hit - (np.multiply(N, 0.001))
            else:
                reflect_orig = hit + (np.multiply(N, 0.001))

            reflect_ray = Ray(reflect_orig, reflect_dir)
            reflect_color = cast_ray(reflect_ray, spheres, lights, depth+1)
        else:
            reflect_color = [0, 0, 0]

        if material.albedo[3] != 0:
            refract_dir = refract(ray.dir, N, material.eta)
            if any(refract_dir) != 0:
                refract_dir /= np.linalg.norm(refract_dir)

            if refract_dir.dot(N) < 0:
                refract_orig = hit - (np.multiply(N, 0.001))
            else:
                refract_orig = hit + (np.multiply(N, 0.001))

            refract_ray = Ray(refract_orig, refract_dir)
            refract_color = cast_ray(refract_ray, spheres, lights, depth+1)
        else:
            refract_color = [0, 0, 0]

        diffuse_light_intensity = 0.
        specular_light_intensity = 0.
        for light in lights:
            light_dir = light.pos - hit
            light_distance = np.linalg.norm(light_dir)
            light_dir /= light_distance

            if light_dir.dot(N) < 0:
                shadow_orig = hit - np.multiply(N, 0.001)
            else:
                shadow_orig = hit + np.multiply(N, 0.001)

            shadow_ray = Ray(shadow_orig, light_dir)

            shadow_res, shadow_hit, shadow_N, shadow_material = scene_intersect(shadow_ray, spheres)

            if shadow_res and np.linalg.norm(shadow_hit - shadow_ray.orig) < light_distance:
                continue

            diffuse_light_intensity += light.intensity * np.max([0., light_dir.dot(N)])

            specular_light_intensity += np.power(np.max([0, -reflect(-light_dir, N).dot(ray.dir)]),
                                                 material.specular_exponent) * light.intensity

        net_color = np.multiply(material.diffuse_color, diffuse_light_intensity) * material.albedo[0]
        net_color += np.multiply([1., 1., 1.], specular_light_intensity) * material.albedo[1]
        net_color += np.multiply(reflect_color, material.albedo[2])
        net_color += np.multiply(refract_color, material.albedo[3])

        return net_color


def render(width, height, sphere, lights, iteration):
    fov = np.pi / 5.
    global framebuffer
    random.seed(iteration)

    for j in range(height):
        randy = random.random() * 0.001
        for i in range(width):
            randx = random.random() * 0.001
            x = (2 * (i + 0.5) / width - 1) * np.tan(fov/2.) * width/height + randy
            y = -(2 * (j + 0.5) / height - 1) * np.tan(fov/2.) + randx

            direction = np.array((x, y, -1))
            direction = direction / np.linalg.norm(direction)

            ray = Ray([0, 0, 0], direction)
            pixel_color = cast_ray(ray, sphere, lights, 0)
            if np.max(pixel_color) > 1:
                pixel_color /= np.max(pixel_color)
            framebuffer[j][i][:] += pixel_color
        if j % 10 == 0:
            print(j)


if __name__ == "__main__":
    REFLECTION_RECURSION_DEPTH = 8
    numrays = 0
    start = time.time()
    red_rubber = Material([0.3, 0.1, 0.1], 10, [0.9, 0.1, 0.0, 0.0], 1.0)
    ivory = Material([0.4, 0.4, 0.3], 50, [0.6, 0.3, 0.1, 0.0], 1.0)
    mirror = Material([1.0, 1.0, 1.0], 1425, [0.0, 10.0, 0.9, 0.0], 1.0)
    glass = Material([0.6, 0.7, 0.8], 125, [0.0, 0.5, 0.1, 0.8], 1.5)

    lights = list()
    lights.append(Light([-20, 20, 20], 1.5))
    lights.append(Light([30, 50, -25], 1.8))
    lights.append(Light([30, 20, 30], 1.7))
    lights.append(Light([30, 2, -20], 1.5))

    spheres = list()
    spheres.append(Sphere([-3, 0, -16], 2, ivory))
    spheres.append(Sphere([-1.0, -1.5, -12], 2, glass))
    spheres.append(Sphere([1.5, -0.5, -18], 3, red_rubber))
    spheres.append(Sphere([7, 5, -18], 4, mirror))

    iterations = 2
    width = 1024
    height = 768
    framebuffer = np.zeros((height, width, 3))
    for i in range(iterations):
        print("Frame {}".format(i+1))
        render(width, height, spheres, lights, i)

    framebuffer /= iterations
    framebuffer *= 255.

    img = Image.fromarray(framebuffer.astype('uint8'), "RGB")
    img.save("./image_{}.bmp".format(iterations), "bmp")
    # print(framebuffer)

    print("Execution took {} seconds.".format(time.time() - start))
    print("{} rays were cast, giving {} rays per second.".format(numrays, numrays/(time.time() - start)))
